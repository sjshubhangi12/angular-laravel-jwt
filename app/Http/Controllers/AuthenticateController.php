<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers;
use JWTAuth;
use Tymon\JWTAuthExceptions\JWTException;
use App\User;




class AuthenticateController extends Controller
{

    public function __construct()
    {
	       // Apply the jwt.auth middleware to all methods in this controller
	       // except for the authenticate method. We don't want to prevent
	       // the user from retrieving their token if they don't already have it
	       $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }

	public function index()
	{
	    // Retrieve all the users in the database and return them
	    $users = User::all();
	    return $users;
	} 

    public function authenticate(Request $request)
    {
        $input = $request->only('email', 'password');
        $jwt_token = null;

        if (!$jwt_token = JWTAuth::attempt($input)) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Email or Password',
            ], Response::HTTP_UNAUTHORIZED);
        }

        return response()->json([
            'success' => true,
            'token' => $jwt_token,
        ]);
    }

    public function getAuthenticatedUser()
	{
		  try {

		    if (! $user = JWTAuth::parseToken()->authenticate()) {
		        return response()->json(['user_not_found'], 404);
		    }

		  } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

		    return response()->json(['token_expired'], $e->getStatusCode());

		  } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

		    return response()->json(['token_invalid'], $e->getStatusCode());

		  } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

		    return response()->json(['token_absent'], $e->getStatusCode());

		  }

		  // the token is valid and we have found the user via the sub claim
		  return response()->json(compact('user'));
	}
}
