<?php
 
namespace App\Http\Controllers\API;
 
use App\Item;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
 
class ItemController extends Controller
{
    public function index()
    {
        return response()->json([
            'error' => false,
            'items'  => Item::all(),
        ], 200);
    }
 
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[ 
            'name' => 'required',
            'quantity' => 'required',
            'price' => 'required',
        ]);
 
        if($validation->fails()){
            return response()->json([
                'error' => true,
                'messages'  => $validation->errors(),
            ], 200);
        }
        else
        {
            $item = new Item;
            $item->name = $request->input('name');
            $item->quantity = $request->input('quantity');
            $item->price = $request->input('price');
            $item->save();
     
            return response()->json([
                'error' => false,
                'item'  => $item,
            ], 200);
        }
    }
 
    public function show($id)
    {
        $item = Item::find($id);
 
        if(is_null($item)){
            return response()->json([
                'error' => true,
                'message'  => "Record with id # $id not found",
            ], 404);
        }
 
        return response()->json([
            'error' => false,
            'item'  => $item,
        ], 200);
    }
 
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(),[ 
            'name' => 'required',
            'quantity' => 'required',
            'price' => 'required',
        ]);
 
        if($validation->fails()){
            return response()->json([
                'error' => true,
                'messages'  => $validation->errors(),
            ], 200);
        }
        else
        {
            $item = Item::find($id);
            $item->name = $request->input('name');
            $item->quantity = $request->input('quantity');
            $item->price = $request->input('price');
            $item->save();
     
            return response()->json([
                'error' => false,
                'item'  => $item,
            ], 200);
        }
    }
 
    public function destroy($id)
    {
        $item = Item::find($id);
 
        if(is_null($item)){
            return response()->json([
                'error' => true,
                'message'  => "Record with id # $id not found",
            ], 404);
        }
 
        $item->delete();
     
        return response()->json([
            'error' => false,
            'message'  => "Item record successfully deleted id # $id",
        ], 200);
    }
}