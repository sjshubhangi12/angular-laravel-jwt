﻿app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

app.controller('authenticateController', function ($scope, $http, API_URL) {
     
    $http({
        method: 'POST',
        url: API_URL + "authenticate",
        date: {email:"ryanchenkie@gmail.com", password:"secret"}
    }).then(function (response) {
        console.log(response);
        $scope.items = response.data.items;
    }, function (error) {
        console.log(error);
        alert('This is embarassing. An error has occurred. Please check the log for details');
    });

    $scope.save = function () {
        var url = API_URL + "authenticate";
        var method = "POST";
        $http({
            method: method,
            url: url,
            data: $.param($scope.auth),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function (response) {
            console.log(response);
            location.reload();
        }), (function (error) {
            console.log(error);
            alert('This is embarassing. An error has occurred. Please check the log for details');
        });
    }
 
});

